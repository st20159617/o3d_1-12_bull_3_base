#version 460

uniform mat4 mvpMatrix;

layout (location=0) in vec3 vertexPos;
layout (location=1) in vec3 vertexTexCoord;

out vec3 texCoord;

void main(void) {

	texCoord = vertexTexCoord;
	gl_Position = mvpMatrix * vec4(vertexPos,1.0);
}

